#lang s-exp lang/plt-pretty-big
(define *num-lignes* 20)
(define *page-break* 0)
(define *weight* 1)
(define *weightH1* 7)
(define *DEBUG* 0)

(define multisubst
  (lambda (new old lat)
    (cond
      ((null? lat) '())
      ((char=? (car lat) old)
       (cons new (multisubst new old (cdr lat))))
      (else
        (cons (car lat) (multisubst new old (cdr lat)))))))

(define nextBreak?
	(lambda (lat)
  	   (cond
	     ((null? lat) #f)
	     ((and (char=? (car lat) '#\newline) (char=? (car lat) '#\newline)) #t)
	     (else #f))))

(define findBreak
  (lambda (lat)
    (cond
      ((null? lat) '())
      ((and (nextBreak? lat) (nextBreak? (cdr lat)))
       (cons '#\< (cons '#\b (cons '#\r (cons '#\> (findBreak (cdr lat)))))))
      (else
        (cons (car lat) (findBreak (cdr lat)))))))

(define findBold
  (lambda (lat endTag)
    (cond
      ((null? lat) '())
      ( (and (char=? (car lat) '#\*) (char=? (cadr lat) '#\*) )
       (if (and endTag endTag) (cons '#\< (cons '#\/ (cons '#\b  (cons '#\>  (findBold (cddr lat) (not endTag)))))) (cons  '#\< (cons '#\b (cons '#\>  (findBold (cddr lat) (not endTag)))))))
      (else
        (cons (car lat) (findBold (cdr lat) endTag))))))

(define findItalic
  (lambda (lat endTag)
    (cond
      ((null? lat) '())
      ( (char=? (car lat) '#\*) 
       (if (and endTag endTag) (cons '#\< (cons '#\/ (cons '#\i  (cons '#\>  (findItalic (cdr lat) (not endTag)))))) (cons  '#\< (cons '#\i (cons '#\>  (findItalic (cdr lat) (not endTag)))))))
      (else
        (cons (car lat) (findItalic (cdr lat) endTag))))))


(define findH3
  (lambda (lat endTag)
    (cond
      ((null? lat) '())
      ( (and (< 1 (length lat)) (char=? (car lat) '#\#) (char=? (cadr lat) '#\#) (char=? (caddr lat) '#\#) )  (set! *weight* 5)
       (if (and endTag endTag) (cons '#\< (cons '#\/ (cons '#\h  (cons '#\3 (cons '#\>  (findH3 (cdddr lat) (not endTag))))))) (cons  '#\< (cons '#\h (cons '#\3 (cons '#\>  (findH3 (cdddr lat) (not endTag))))))))
      (else
        (cons (car lat) (findH3 (cdr lat) endTag))))))


(define findH2
  (lambda (lat endTag)
    (cond
      ((null? lat) '())
      ( (and (< 1 (length lat)) (and (char=? (car lat) '#\#) (char=? (cadr lat) '#\#) )) (set! *weight* 6)
       (if (and endTag endTag) (cons '#\< (cons '#\/ (cons '#\h  (cons '#\2 (cons '#\>  (findH2 (cddr lat) (not endTag))))))) (cons  '#\< (cons '#\h (cons '#\2 (cons '#\>  (findH2 (cddr lat) (not endTag))))))))
      (else
        (cons (car lat) (findH2 (cdr lat) endTag))))))

(define findH1
  (lambda (lat endTag)
    (cond
      ((null? lat) '())
      ( (char=? (car lat) '#\#) (set! *weight* 7) 
       (if (and endTag endTag) (cons '#\< (cons '#\/ (cons '#\h (cons '#\1  (cons '#\>  (findH1 (cdr lat) (not endTag))))))) (cons  '#\< (cons '#\h (cons '#\1 (cons '#\>  (findH1 (cdr lat) (not endTag))))))))
      (else
        (cons (car lat) (findH1 (cdr lat) endTag))))))

(define NBreaks
   (let ((str "") (ret "")) 
        (lambda (n)
                (set! *page-break* 1)
		(do ((i 0 (+ i 1)))
		  ((= i n) (set! ret str) (set! str "") ret)
		  (set! str (string-append str "&nbsp;<br>"))))))  

(define parse
	(lambda (lat num-ligne)
		(cond
                  ((null? lat) '"<br>")
                  ((char=? (car lat) '#\return) (set! *weight* 1) '"<br>")
		((and (char=? (car lat) '#\= ) (char=? (cadr lat) '#\=)) (NBreaks (- *num-lignes* num-ligne)))
		((char=? (car lat) '#\> ) (set! *weight* 3) (string-append "<blockquote>" (list->string (cdr lat)) "</blockquote>"))
		( (and (< 1 (length lat)) (and (char=? (car lat) '#\!) (char=? (cadr lat) '#\[) ) ) (parseImage (list->string lat))) 
		(else (set! *weight* (ceiling (/ (length lat) 70))) (list->string (findH1 (findH2 (findH3 (findItalic (findBold  lat #f) #f) #f) #f) #f) )))
	))

(define read-next-line-iter 
   (let ((line "") (line-count 0))
      (lambda (file out) (begin
	     (set! line (read-line file))
             (set! *weight* 0)
	     (if (eof-object? line)
	       'done
 	       (begin
               (if (= *DEBUG* 1) (write-string (number->string line-count) outport))
	       (write-string (parse (string->list line) line-count)  outport)
	       (write-string (string-append '"\n" )  outport)
	       (if (= 1 *page-break*) (begin (set! *page-break* 0) (set! line-count 0)) (set! line-count (+ line-count *weight*)))
	       (set! line-count (modulo line-count *num-lignes*))
	       (read-next-line-iter file out))
		)))))

(define nextCarret?
	(lambda (lat)
  	   (cond
	     ((null? lat) #f)
	     ((char=? (car lat) '#\]) #t)
	     (else #f))))

(define parseImage
	(lambda (str)
         (string-append "<img height='360' src=\"" (substring str (+ 2 (findCarret (string->list str))) (- (string-length str) 2)) "\"/>")
  	   ))

(define findCarret
  (let ((c 0) (ret 0))
   (lambda (lat)
    (cond
      ((null? lat) '())
      ((nextCarret? lat)
       (begin
         (set! ret c)
         (set! c 0)
         ret
          ))
      (else
        (begin
         (set! c (+ c 1))
        (findCarret (cdr lat))
        ))))))

(define outport (open-output-file "out.html"))
(write-string "<?xml version='1.0' encoding='utf-8'?> <html xmlns='http://www.w3.org/1999/xhtml'> <head> <meta charset='utf-8'/> <title>Contenu</title> <link rel='stylesheet' type='text/css' href='../css/epub-spec.css'/></head><body>" outport)
(define inport (open-input-file "walden.md"))
(read-next-line-iter inport outport)
(write-string "</body> </html>" outport)
(close-input-port inport)
(close-output-port outport)
(exit)
