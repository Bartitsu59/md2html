# md2html

This program is written in scheme and runs in Dr Racket in "Pretty Big" language mode.
The source declares this in a pragma, so a simple :

`racket conv.scm`
	

should do the job.

What it does :  it converts an ebook or document written in Markdown (.md file extension) to regular HTML.
The public domain "Walden" book is provided as an example input.


